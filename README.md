# Registry Pull

A simple `.gitlab-ci.yml` pulling images from multiple registries.

## Private Registries

This repository pulls from private registries make sure you have
[DOCKER_AUTH_CONFIG](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#determining-your-docker_auth_config-data) set up, for example:

```json
{
    "auths": {
        "https://index.docker.io/v1/": {
            "auth": "xxxx"
        },
        "registry.gitlab.com": {
            "auth": "xxxx"
        }
    }
}
```
